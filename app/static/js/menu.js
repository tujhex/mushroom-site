/**
 * Created by biospore on 7/1/16.
 */
function init() {
    document.body.onclick = function(e) {
        var target = e.target || e.srcElement;
        
        if (target.id == 'tbutton'){
            var menu = document.getElementById('menu');
            var lst = menu.className.split(' ');
            var flag = true;
            var cn = [];
            for (var i = 0; i<lst.length; i++){
                if (lst[i] == 'collapse'){
                    flag = false;
                }
                else{
                    cn.push(lst[i]);
                }
            }
            if (flag == true){
                cn.push('collapse');
            }
            menu.className = cn.join(' ');
        }
    }
}