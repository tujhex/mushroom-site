import os


def get_menu():
    # check block
    current_path = os.getcwd()
    if current_path.split('/')[-1] != 'app':
        if "app" in os.listdir(current_path):
            os.chdir("app")
        else:
            raise FileNotFoundError(current_path + "/app")
    # new path block
    new_wd = os.getcwd() + '/templates'
    # processing block
    paths = process_dir(new_wd, '/')
    return paths


def process_dir(wd, rp):
    paths = dict()
    dir_content = os.listdir(wd)
    for item in dir_content:
        if os.path.isdir(wd + "/" + item):
            if item[0] == '.':
                continue
            paths.update(process_dir(wd + "/" + item, rp + item + '/'))
        else:
            paths.update({item[:-5].title().replace('_', ' '): rp + item[:-5]})
    return paths


def update_config_paths(keywords):
    paths = get_menu()
    keywords.STATIC_URLS.update(paths)
    sorted(keywords.STATIC_URLS)


def check_if_page_exists(path):
    return os.path.exists(os.getcwd() + "/templates/" + path)
