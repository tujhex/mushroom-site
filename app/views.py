from app import app, scripts, config
from flask import render_template, abort


STATIC = config.Keywords()


@app.route("/")
@app.route("/home")
@app.route("/index")
def index():
    scripts.update_config_paths(STATIC)
    return render_template("/index.html", menu=STATIC.STATIC_URLS, active="Index")


@app.route("/<page_path>")
def page(page_path):
    if not scripts.check_if_page_exists(page_path + ".html"):
        print(page_path + ".html")
        abort(404)
    scripts.update_config_paths(STATIC)
    return render_template(page_path + ".html", menu=STATIC.STATIC_URLS, active=page_path.title())


@app.errorhandler(404)
def error_handler(err):
    scripts.update_config_paths(STATIC)
    return render_template(".error/404.html", error_text=err, menu=STATIC.STATIC_URLS), 404
